import React from "react";
import AddUser from "./component/Users/AddUser";
import ListUser from "./component/Users/ListUser";
import { useState } from "react";

function App() {
  const [dataUser, setDataUser] = useState([]);

  const handleDataUser = (uName, uAge) => {
    setDataUser((prevData) => {
      return [
        ...prevData,
        { username: uName, age: uAge, id: Math.random().toString() },
      ];
    });
  };

  const handleDeleteUser = (id) => {
    setDataUser((prevData) => {
      const delUser = prevData.filter((user) => user.id !== id);
      return delUser;
    });
  };

  return (
    <div>
      <AddUser onHandleDataUser={handleDataUser} />
      <ListUser listDataUser={dataUser} onHandleDelete={handleDeleteUser} />
    </div>
  );
}

export default App;
