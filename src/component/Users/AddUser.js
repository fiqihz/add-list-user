import { useState, Fragment, useRef } from "react";
import classes from "./AddUser.module.css";
import Card from "../UI/Card";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";

const AddUser = (props) => {
  const [error, setError] = useState();

  const nameInputRef = useRef();
  const ageInputRef = useRef();

  const handleSubmitUser = (event) => {
    event.preventDefault();
    const enteredName = nameInputRef.current.value;
    const enteredAge = ageInputRef.current.value;
    if (enteredName === "" && enteredAge === "") {
      setError({
        title: "Invalid input",
        message: "Please enter a name and age (non-empty values)",
      });
      return;
    } else if (
      (enteredName === "" && enteredAge !== "") ||
      (enteredName !== "" && enteredAge === "")
    ) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid name and age (non-empty values)",
      });
      return;
    } else if (enteredAge < 1) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid age (>0)",
      });
      return;
    } else {
      props.onHandleDataUser(enteredName, enteredAge);
      nameInputRef.current.value = "";
      ageInputRef.current.value = "";
    }
  };

  const handleError = () => {
    setError(null);
  };

  return (
    <Fragment>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={handleError}
        />
      )}
      <Card className={classes.input}>
        <form onSubmit={handleSubmitUser}>
          <label>Username</label>
          <input type="text" ref={nameInputRef} />
          <label>Age (Years)</label>
          <input type="number" ref={ageInputRef} />
          <Button type="submit">Add User</Button>
        </form>
      </Card>
    </Fragment>
  );
};

export default AddUser;
