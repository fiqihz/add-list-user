import classes from "./ListUser.module.css";
import Card from "../UI/Card";

const ListUser = (props) => {
  const deleteUser = (id) => {
    props.onHandleDelete(id);
  };

  return (
    <Card className={props.listDataUser.length === 0 ? "" : classes.users}>
      <ul>
        {props.listDataUser.map((user) => (
          <li
            key={user.id}
            onClick={() => {
              deleteUser(user.id);
            }}
          >
            {user.username} ({user.age} years old)
          </li>
        ))}
      </ul>
    </Card>
  );
};

export default ListUser;
